console.log("    *"); // 4 spaces | 1 star
console.log("   ***"); // 3 spaces | 3 stars
console.log("  *****"); // 2 spaces | 5 stars
console.log(" *******"); // 1 space | 7 stars
console.log("*********"); // 0 space | 9 stars

let lineNumber = 30;
let starNumber = 1;
let spaceNumber = lineNumber - 1;

for (let line = 0; line < lineNumber; line++) {
    let stars = "";

    // stars += " ".repeat(spaceNumber);
    for (let col = 0; col < spaceNumber; col++) {
        stars += " ";

    }

    // stars += "*".repeat(spaceNumber);
    for (let col = 0; col < starNumber; col++) {
        stars += "*";

    }
    console.log(stars);
    spaceNumber--;
    starNumber += 2;

}
